//
//  TimeFilterViewController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/26/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

class TimeFilterViewController: UIViewController {

    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    var datePicker: UIDatePicker!
    var fromDate: Date = Date().minusMonths(m: 1)
    var toDate: Date = Date()
    
    var completion: ((_ startDate: Date, _ endDate: Date) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        
        title = "Tuỳ chọn thời gian"
        
        let confirmItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_tick_all"), style: .plain, target: self, action: #selector(doConfirm))
        navigationItem.rightBarButtonItem = confirmItem
        
        fromDateTextField.text = fromDate.toString()
        toDateTextField.text = toDate.toString()
        
        datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.locale = Locale(identifier: "vi_VN")
        datePicker.calendar = Calendar(identifier: .gregorian)
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        let cancelItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(doCancelItem(_:)))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "Chọn", style: .plain, target: self, action: #selector(doDoneItem(_:)))
        toolBar.setItems([cancelItem, spaceItem, doneItem], animated: false)
        
        fromDateTextField.inputAccessoryView = toolBar
        fromDateTextField.inputView = datePicker
        
        toDateTextField.inputAccessoryView = toolBar
        toDateTextField.inputView = datePicker
    }
    
    func doCancelItem(_ sender: Any) {
        fromDateTextField.resignFirstResponder()
        toDateTextField.resignFirstResponder()
    }
    
    func doDoneItem(_ sender: Any) {
        
        if fromDateTextField.isEditing {
            fromDate = datePicker.date
            fromDateTextField.text = fromDate.toString()
        } else if toDateTextField.isEditing {
            toDate = datePicker.date
            toDateTextField.text = toDate.toString()
        }
        
        fromDateTextField.resignFirstResponder()
        toDateTextField.resignFirstResponder()
    }
    
    func dateChanged(_ sender: UIDatePicker) {
        
    }

    func doConfirm() {
        dismiss(animated: true, completion: nil)
        
        if completion != nil {
            completion!(fromDate, toDate)
        }
    }

}
