//
//  ISGTabBarController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

class ISGTabBarController: UITabBarController {
    
    var menuViewController: MenuViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        NotificationCenter.default.addObserver(self, selector: #selector(doShowOrHideMenu(_:)), name: .menuButtonClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cartHasChanged(_:)), name: .cartHasChanged, object: nil)
        
        updateBadgetCount()
//        setupMenu()
     
    }
    
    func setupMenu() {
        if menuViewController == nil {
            
            menuViewController = MenuViewController.viewController(.home) as! MenuViewController
            menuViewController.view.frame = UIScreen.main.bounds
            
            menuViewController.doCloseMenu = { (category: CategoryModel?) in
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuViewController.tableView.frame = CGRect(x: 100 - UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width - 100, height: UIScreen.main.bounds.height)
                    self.menuViewController.closeButton.alpha = 0.0
                }, completion: { (finished: Bool) in
                    self.menuViewController.removeFromParentViewController()
                    self.menuViewController.view.removeFromSuperview()
                    
                    if let item = category {
                        if let navigationView = self.viewControllers?[0] as? UINavigationController {
                            if let homeView = navigationView.viewControllers[0] as? HomeViewController {
                                homeView.categoryDidChanged(item: item)
                            }
                        }
                    }
                })
            }
        }
    }
    
    func updateBadgetCount() {
//        let count = DataManager.shared.countProductsInCart()
//        if count > 0 {
//            self.tabBar.items![2].badgeValue = String(count)
//        } else {
//            self.tabBar.items![2].badgeValue = nil
//        }
    }
    
    
    func cartHasChanged(_ notification: NSNotification) {
        updateBadgetCount()
    }
    
    func doShowOrHideMenu(_ notification: NSNotification) {
        
        if menuViewController != nil && !menuViewController.isShown {
            
//            menuViewController.dataSource = DataManager.shared.categories
            addChildViewController(menuViewController)
            view.addSubview(menuViewController.view)
            menuViewController.tableView.frame = CGRect(x: 100 - UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width - 100, height: UIScreen.main.bounds.height)
            menuViewController.closeButton.alpha = 0.0
            
            
            UIView.animate(withDuration: 0.5, animations: {
                self.menuViewController.tableView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 100, height: UIScreen.main.bounds.height)
                self.menuViewController.closeButton.alpha = 1.0
            })
        }
    }
}
