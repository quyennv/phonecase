//
//  ISGVideoPlayerViewController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ISGVideoPlayerViewController: AVPlayerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customUnlockOrientation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        customLockOrientation()
    }
    
}

extension ISGVideoPlayerViewController {
    
    func customLockOrientation() {
        
        Constant.appDelegate.isPlayingVideo = false
        UIApplication.shared.statusBarOrientation = .portrait
        UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientationMask.portrait.rawValue), forKey: "orientation")
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()

    }
    
    func customUnlockOrientation() {
        
        Constant.appDelegate.isPlayingVideo = true
        UIApplication.shared.statusBarOrientation = .portrait

    }
}

