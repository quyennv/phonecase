//
//  MenuViewController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "MenuViewCell"
private let reuseHeaderIdentifier = "MenuHeaderView"

class MenuViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var isShown = false
    var doCloseMenu: ((_ category: CategoryModel?) -> Void)?
    
    var dataSource: [CategoryModel]! {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setupUI() {
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        
    }
    

    
    @IBAction func doCloseMenu(_ sender: Any) {
        if doCloseMenu != nil {
            doCloseMenu!(nil)
        }
    }
    
}

extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! MenuViewCell
        
        cell.item = dataSource[indexPath.section].subCate[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource[section].isExpand {
            return dataSource[section].subCate.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataSource != nil {
            return dataSource.count
        }
        return 0
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! MenuHeaderView
        
        headerView.item = dataSource[section]
        headerView.section = section
        headerView.delegate = self
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if doCloseMenu != nil {
            let item = dataSource[indexPath.section].subCate[indexPath.row]
            doCloseMenu!(item)
        }
    }
}

extension MenuViewController: MenuHeaderViewDelegate {
    func menuHeaderViewDidSelectExpandButtonAtSection(_ section: Int) {
        for index in 0...(dataSource.count - 1) {
            if index != section {
                dataSource[index].isExpand = false
            }
        }
        dataSource![section].isExpand = !dataSource![section].isExpand
        tableView.reloadData()
    }
    
    func menuHeaderViewDidSelectAtSection(_ section: Int) {
        doCloseMenu?(dataSource![section])
    }
}
