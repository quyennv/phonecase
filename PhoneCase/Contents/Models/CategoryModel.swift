//
//  CategoryModel.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryModel: NSObject {

    var id = 0
    var name = ""
    var subCate = [CategoryModel]()
    var parentCate: CategoryModel!
    
    var isExpand = false
    
    override init() {
        
    }
    
    init(_ json: JSON, _ cate: CategoryModel? = nil) {
        super.init()
        id = json["id"].intValue
        name = json["name"].stringValue
        for subjson in json["subCate"].arrayValue {
            subCate.append(CategoryModel(subjson, self))
        }
        if let _cate = cate {
            parentCate = _cate
        }
        
    }
    
}
