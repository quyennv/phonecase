//
//  MenuHeaderView.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 11/15/16.
//  Copyright © 2016 Phong Nguyen Nam. All rights reserved.
//

import UIKit

protocol MenuHeaderViewDelegate {
    func menuHeaderViewDidSelectExpandButtonAtSection(_ section: Int)
    func menuHeaderViewDidSelectAtSection(_ section: Int)
}

class MenuHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expandLabel: UILabel!

    var item: CategoryModel? {
        didSet {
            setupUI()
        }
    }
    var section: Int = 0
    var delegate: MenuHeaderViewDelegate?
    
    func setupUI() {
        if let category = item {
            titleLabel.text = category.name
            
            if category.subCate.count > 0 {
                expandLabel.isHidden = false
            } else {
                expandLabel.isHidden = true
            }
            
            if category.isExpand {
                expandLabel.text = "-"
            } else {
                expandLabel.text = "+"
            }
        }
    }
    
    @IBAction func doSelectButton(_ sender: Any) {
        delegate?.menuHeaderViewDidSelectAtSection(section)
    }
    @IBAction func doExpandButton(_ sender: Any) {
        if delegate != nil {
            delegate!.menuHeaderViewDidSelectExpandButtonAtSection(section)
        }
    }
}
