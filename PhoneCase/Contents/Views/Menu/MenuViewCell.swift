//
//  MenuViewCell.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 11/15/16.
//  Copyright © 2016 Phong Nguyen Nam. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expandLabel: UILabel!
    
    var item: CategoryModel?{
        didSet {
            setupUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI() {
        if let category = item {
            titleLabel.text = ">   " + category.name
        }
    }
    
}
