//
//  ISGTextField.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/26/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

class ISGTextField: UITextField {
    
    @IBInspectable var lineColor: UIColor {
        get {
            return Constant.AppColor.buttonColor
        }
        set (color) {
            self.underlineColor = color
        }
    }
    @IBInspectable var leftImageView: UIImage? {
        get {
            return nil
        }
        set (image) {
            self.leftImage = image
        }
    }
    @IBInspectable var rightImageView: UIImage? {
        get {
            return nil
        }
        set (image) {
            self.rightImage = image
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor {
        get {
            return UIColor.lightGray
        }
        set (color) {
            self._placeHolderColor = color
        }
    }
    
    var _placeHolderColor: UIColor? = UIColor.lightGray {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var leftImage: UIImage? {
        didSet {
            addLeftView(image: leftImage!)
        }
    }
    var rightImage: UIImage? {
        didSet {
            addRightView(image: rightImage!)
        }
    }
    
    var underlineColor: UIColor! = Constant.AppColor.buttonColor {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let startingPoint = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 2.0
        
        underlineColor.setStroke()
        
        path.stroke()
        if placeholder != nil {
            self.attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSForegroundColorAttributeName: _placeHolderColor!])
        } else {
            self.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: _placeHolderColor!])
        }
        
        backgroundColor = UIColor.clear
        
    }
    
    override func awakeFromNib() {
        self.spellCheckingType = .no
        self.autocorrectionType = .no
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        var rightSize: CGSize!
        if rightImage != nil {
            rightSize = rightImage!.size
        }
        
        if leftImage != nil && rightImage != nil {
            return CGRect(x: bounds.origin.x + leftImage!.size.width + 8, y: bounds.origin.y, width: bounds.size.width - leftImage!.size.width - rightSize.width - 8, height: bounds.size.height)
        }
        if leftImage != nil && rightImage == nil {
            return CGRect(x: bounds.origin.x + leftImage!.size.width + 8, y: bounds.origin.y, width: bounds.size.width - leftImage!.size.width - 8, height: bounds.size.height)
        }
        if rightImage != nil && leftImage == nil {
            return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width - rightSize.width, height: bounds.size.height)
        }
        return bounds.insetBy(dx: 0, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rightSize: CGSize!
        if rightImage != nil {
            rightSize = rightImage!.size
        }
        
        if leftImage != nil && rightImage != nil {
            return CGRect(x: bounds.origin.x + leftImage!.size.width + 8, y: bounds.origin.y, width: bounds.size.width - leftImage!.size.width - rightSize.width - 8, height: bounds.size.height)
        }
        if leftImage != nil && rightImage == nil {
            return CGRect(x: bounds.origin.x + leftImage!.size.width + 8, y: bounds.origin.y, width: bounds.size.width - leftImage!.size.width - 8, height: bounds.size.height)
        }
        if rightImage != nil && leftImage == nil {
            return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width - rightSize.width, height: bounds.size.height)
        }
        return bounds.insetBy(dx: 0, dy: 0)
    }
    
    private func addLeftView(image: UIImage) {
        leftViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        imageView.image = image
        imageView.contentMode = .left
        leftView = imageView
    }
    
    private func addRightView(image: UIImage) {
        rightViewMode = .always
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        rightView = imageView
    }
    
    func isNilOrEmpty() -> Bool {
        if self.text == nil || self.text!.isEmpty {
            return true
        }
        return false
    }
    
}

