//
//  ISGAlertController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit


typealias ISGAlertControllerCompletion = (_ alertView: ISGAlertController, _ buttonIndex: Int) -> Void
typealias ISGAlertControllerShownCompletion = () -> Void
typealias ISGAlertControllerRemovedCompletion = () -> Void


class ISGAlertController: NSObject {
    
    var tag: Int = 0
    var title: String?
    var message: String?
    var buttonTitles: Array<String>!
    var cancelButtonIndex: Int = 0
    var isShowing = false
    var autoRemoving = false
    var autoRemovedCompletion: ISGAlertControllerRemovedCompletion!
    
    private var hasNotification = false
    var completion: ISGAlertControllerCompletion!
    private var alert: AnyObject?
    
    init(title: String?, message: String?, completion: ISGAlertControllerCompletion!, cancelButtonTitle: String?, otherButtonTitle: String?) {
        super.init()
        
        DispatchQueue.main.async {
            
            self.title = title
            self.message = message
            
            self.buttonTitles = Array<String>()
            if #available(iOS 8.0, *) {
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                self.alert = alertController
                
                if cancelButtonTitle != nil && (cancelButtonTitle?.characters.count)! > 0 {
                    self.cancelButtonIndex = self.buttonTitles.count
                    self.buttonTitles.append(cancelButtonTitle!)
                }
                
                if otherButtonTitle != nil && (otherButtonTitle?.characters.count)! > 0 {
                    self.buttonTitles.append(otherButtonTitle!)
                }
                for (index, element) in self.buttonTitles.enumerated() {
                    
                    alertController.addAction(UIAlertAction(title: element, style: (index == self.cancelButtonIndex) ? .cancel : .default, handler: { (action: UIAlertAction) in
                        self.isShowing = false
                        
                        if completion != nil {
                            
                            completion(self, index)
                        }
                    }))
                }
            } else {
                
                let alt = UIAlertView()
                alt.delegate = self
                alt.title = title!
                alt.message = message
                if cancelButtonTitle != nil && (cancelButtonTitle?.characters.count)! > 0 {
                    self.cancelButtonIndex = 0
                    alt.cancelButtonIndex = 0
                    alt.addButton(withTitle: cancelButtonTitle)
                    self.buttonTitles.append(cancelButtonTitle!)
                }
                
                if otherButtonTitle != nil && (otherButtonTitle?.characters.count)! > 0 {
                    alt.addButton(withTitle: otherButtonTitle!)
                    self.buttonTitles.append(otherButtonTitle!)
                }
                
                if completion != nil {
                    self.completion = completion
                }
                self.alert = alt
            }
        }
        
        autoRemoving = false
    }
    
    func setAutoRemoving(autoRemove: Bool, autoRemovedCompletion completion: @escaping ISGAlertControllerRemovedCompletion) {
        autoRemoving = autoRemove
        if autoRemovedCompletion != nil {
            autoRemovedCompletion = completion
        }
    }
    
    func clear() {
        if isShowing {
            remove(completion: nil)
        }
        
        if hasNotification {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
            hasNotification = false
        }
        
        autoRemovedCompletion = nil
        if buttonTitles != nil {
            buttonTitles?.removeAll()
            buttonTitles = nil
        }
        alert = nil
        completion = nil
    }
    
    deinit {
        
        clear()
    }
    
    func show(completion: @escaping ISGAlertControllerShownCompletion) {
        
        if let window = UIApplication.shared.keyWindow {
            var viewController = window.rootViewController
            while viewController!.presentedViewController != nil {
                viewController = viewController?.presentedViewController
            }
            show(viewController: viewController!, completion: completion)
        }
    }
    
    func show(viewController: UIViewController!, completion: ISGAlertControllerShownCompletion!) {
        
        DispatchQueue.main.async {
            
            NotificationCenter.default.addObserver(self, selector: #selector(ISGAlertController.didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
            self.hasNotification = true
            self.isShowing = true
            
            if #available(iOS 8.0, *) {
                
                let ac = self.alert as! UIAlertController
                
                if viewController != nil {
                    viewController.present(ac, animated: true, completion: {
                        if completion != nil {
                            let _ = completion
                        }
                    })
                }
                
            } else {
                let av = self.alert as! UIAlertView
                av.show()
                if completion != nil {
                    let _ = completion
                }
            }
        }
    }
    
    func remove(completion: ISGAlertControllerRemovedCompletion!) {
        
        DispatchQueue.main.async {
            
            if self.hasNotification {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
                self.hasNotification = false
            }
            
            if #available(iOS 8.0, *) {
                
                let ac = self.alert as! UIAlertController
                ac.dismiss(animated: true, completion: completion)
                
            } else {
                
                let av = self.alert as! UIAlertView
                av.dismiss(withClickedButtonIndex: -1, animated: false)
                if completion != nil {
                    let _ = completion
                }
            }
            self.isShowing = false
            
        }
    }
    
    func didEnterBackground(notification: Notification) {
        if autoRemoving && isShowing {
            remove(completion: autoRemovedCompletion)
        }
    }
    
}
