//
//  ISGNavigationController.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

class ISGNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doCloseItem() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ISGNavigationController {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if viewController.navigationItem.backBarButtonItem == nil {
            let backBarItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            viewController.navigationItem.backBarButtonItem = backBarItem
        }
        
//        if  viewController.isKind(of: CartViewController.self) ||
//            viewController.isKind(of: TimeFilterViewController.self) ||
//            viewController.isKind(of: NewCommunityViewController.self) ||
//            viewController.isKind(of: SettingFilterViewController.self) ||
//            viewController.isKind(of: OrderFilterViewController.self) ||
//            viewController.isKind(of: NewBrandViewController.self) ||
//            viewController.isKind(of: InventoryFilterViewController.self)
//            {
//            let closeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_close"), style: .plain, target: self, action: #selector(doCloseItem))
//            viewController.navigationItem.leftBarButtonItem = closeItem
//        }
        
    }
    
}
