//
//  ISGMessageComposer.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 5/19/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit
import Foundation
import MessageUI

class ISGMessageComposer: NSObject, MFMessageComposeViewControllerDelegate {
    
    func canSendText()  -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    func configMessageComposeViewController(_ recipent: String) -> MFMessageComposeViewController {
        let messageComposeViewController = MFMessageComposeViewController()
        messageComposeViewController.messageComposeDelegate = self
        messageComposeViewController.recipients = [recipent]
        return messageComposeViewController
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
