//
//  Constant.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

struct Constant {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static let limit = 20
    static let appFeatureDomain = "APP_DOIMAIN_CONFIG"
    static let appFeaturePromoteDomain = "ishopgo.com"
    
    struct AppColor {
        static let green = UIColor(hex: "#4caf50")
        static let appColor = UIColor(hex: "#4caf50")
        static let buttonColor = UIColor(hex: "#2f7499")
        static let appFeatureCellColor = UIColor(hex: "#E6E6E7")
        static let lightGrayText = UIColor(hex: "#333333")
    }
    
    struct Api {
        static let baseUrl = "http://ishopgo.com/api/v1/"
        static let host = "http://ishopgo.com/"
        
        struct Path {
            
            // Auth
            static let login = "login"
            static let getDomains = "domains"
            static let register = "register"
            static let forgotPassword = "user/forgotPassword"
            static let getIntroduction = "introduction-post"
            static let logout = "logout"
            
            // Home
            static let getCategories = "categories"
            static let getBrands = "departments"
            static let getProductsFromCategory = "category-products"
            static let getProductsFromBrand = "department-products"
            static let getProductDetail = "product"
            static let searchProduct = "search-product"
            static let featuredProducts = "product/featured"
            
            // Album
            static let getProductAlbumImage = "album/item"
            static let likeProductAlbumImage = "like-album-item"
            static let commentProductAlbumImage = "comment-album-item"
            static let shareProductAlbumImage = "share-album-item"
            
            // Setting
            static let getProfileInfo = "get-profile"
            static let editProfile = "update-profile"
            static let getOrders = "user-orders"
            static let getOrderdetail = "order-detail"
            static let cancelOrder = "destroy-order-customer"
            static let getProviders = "providers"
            
            // Income
            static let getIncome = "revenue"
            
            // Notification
            static let getNotifications = "get-notifications"
            static let readNotification = "read-notification"
            static let getNotificationDetail = "get-notification"
            static let getNumberUnreadNotification = "number-unread-notification"
            
            // Hotline
            static let getHotlines = "hotlines"
            static let addHotline = "add-hotline"
            static let deleteHotline = "delete-hotline"
            static let editHotline = "edit-hotline"
            
            // Manage shop
            static let manageShop = "shops"
            static let changeShopTakeCareTime = "update-date-shop-care"
            static let getShopNotes = "shops-notes"
            static let addShopNote = "update-note-shop-care"
            static let updateShopTakeCareStatus = "change-complete-shop"
            
            // Manage distributor
            static let manageDistributor = "contributors"
            static let changeDistributorTakeCareTime = "update-date-contributor-care"
            static let getDistributorNotes = "contributor-notes"
            static let addDistributorNote = "update-note-contributor-care"
            static let updateDistributorTakeCareStatus = "change-complete-contributor"
            
            // cashbook
            static let cashbook = "collect-spend"
            
            // customer service
            static let getCustomers = "customers-care"
            static let getCustomerDetail = "customer"
            static let addCustomer = "create-potential-customers"
            static let updateCustomerTime = "update-date-customer-care"
            static let getCustomerNote = "customers-notes"
            static let addCustomerNote = "update-note-customer-care"
            static let updateCustomerStatus = "change-complete-customer"
            
            // Vip
            static let findUser = "find-user"
            static let updateRank = "update-rank-account"
            static let activeAccount = "update-status-account"
            
            // training
            static let getTrainings = "training"
            static let getTrainingDetail = "training-post"
            
            // Community
            static let getCommunities = "social-posts"
            static let getSocialDetail = "social-post"
            static let likeCommunity = "social-post-like"
            static let deleteCommunity = "social-delete-post"
            static let updateCommunity = "social-update-post"
            static let newCommunity = "add-post"
            static let getSocialComment = "social-comments"
            static let addSocialComment = "add-comment"
            
            // Push notification
            static let addPushNotification = "add-notification"
            
            // Change password
            static let changePassword = "change-password"
            
            
            // Order
            static let getCities = "get-cities"
            static let addCart = "add-cart"
            
            // Manage products
            static let manageProducts = "provider-products"
            
            // Hotface
            static let getHotfaces = "hotfaces"
            static let getHotFaceTransactions = "hotface-transactions"
            static let updateHotFaceStatus = "update-status-hotface-transaction"
            static let createHotFaceTransaction = "create-hotface-transaction"
            static let updateHotFaceTransaction = "update-hotface-transaction"
            static let deleteHotFaceTransaction = "delete-hotface-transaction"
            
            // Domain + Adword
            static let getDomain = "domains"
            static let createDomain = "create-domain"
            static let editDomain = "edit-domain"
            static let deleteDomain = "delete-domain"
            
            static let getAdword = "adwords"
            static let createAdword = "create-adword"
            static let editAdword = "edit-adword"
            static let deleteAdword = "delete-adword"
            
            
            static let create_brand = "create-brands"
            static let update_brand = "update-brands/"
            static let delete_brand = "delete-brands/"
            static let getJoinedShop = "shop-join"
            
            static let getNews = "news"
            static let deleteNews = "delete-news/"
            static let getPolicies = "policies"
            static let deletePolicy = "delete-policies/"
        }
    }
    
}

enum AppErrorCode: Int {
    case network = 999
    case unAuthorized = 401
    case timeout = -1001
    case serverConnection = -1004
    case serverError = 500
}

enum UserRole: Int, EnumEnumerable {
    
    case admin = 999
    case staffESG = 998
    case shopOwner = 99
    case staff = 1
    case distributor = 2
    case shop = 3
    case buyer = 4
    case provider = 5
    case collaborator = 6
    case staffSale = 7
    case staffShop = 8
    case staffWH = 9
    
}

enum SortType: String {
    case ascending = "asc"
    case descending = "desc"
}

enum ProductSortType: Int, EnumEnumerable {
    case lowestPrice, highestPrice, aName, zName
    
    var title: String {
        switch self {
        case .lowestPrice: return "Giá từ thấp đến cao"
        case .highestPrice: return "Giá từ cao đến thấp"
        case .aName: return "Tên sản phẩm từ A - Z"
        case .zName: return "Tên sản phẩm từ Z - A"
        }
    }
    
    var key: String {
        switch self {
        case .lowestPrice: return "price"
        case .highestPrice: return "price"
        case .aName: return "name"
        case .zName: return "name"
        }
    }
    
    var sort: SortType {
        switch self {
        case .lowestPrice: return .ascending
        case .highestPrice: return .descending
        case .aName: return .ascending
        case .zName: return .descending
        }
    }
    
}

enum PaymentType: Int, EnumEnumerable {
    case cod, bank, company
    
    var title: String {
        switch self {
        case .cod: return "Thanh toán khi nhận hàng COD"
        case .bank: return "Chuyển khoản ngân hàng"
        case .company: return "Thanh toán tại công ty"
        }
    }
    
    var id: Int {
        switch self {
        case .cod: return 1
        case .bank: return 2
        case .company: return 11
        }
    }
}

enum SocialType: Int, EnumEnumerable {
    case community = 0
    case chat = 1
}

enum OrderStatus: Int, EnumEnumerable {
    
    case all
    
    case notProcessed
    case destroyOrder
    case processing
    case notReceivePackage
    case payBack
    case processed
    case deposit
    case backAgency
    case gift
    

    var id: Int {
        switch self {
        case .all: return 0
        case .processed: return 5
        case .notProcessed: return 1
        case .payBack: return 15
        case .processing: return 13
        case .notReceivePackage: return 6
        case .destroyOrder: return 8
        case .backAgency: return 35
        case .deposit: return 34
        case .gift: return 33
        }
    }
    
    var key: String {
        switch self {
        case .all: return "ALL"
        case .processed: return "ORDER_PROCESSED"
        case .notProcessed: return "ORDER_NOT_PROCESSED"
        case .payBack: return "ORDER_PAY_BACK"
        case .processing: return "ORDER_PROCESSING"
        case .notReceivePackage: return "NOT_RECEIVE_PACKAGE"
        case .destroyOrder: return "WRONG_DESTROY_ORDER"
        case .backAgency: return "ORDER_BACK_AGENCY"
        case .deposit: return "ORDER_DEPOSIT"
        case .gift: return "ORDER_GIFT"
        }
    }
    
    var title: String {
        switch self {
        case .all: return "Tất cả"
        case .processed: return "Đã thu tiền"
        case .notProcessed: return "Chưa xử lý"
        case .payBack: return "Đơn hàng trả"
        case .processing: return "Đang giao hàng"
        case .notReceivePackage: return "Không nhận hàng"
        case .destroyOrder: return "Huỷ đơn sai"
        case .backAgency: return "Trả đại lý"
        case .deposit: return "Đơn ký gửi"
        case .gift: return "Quà tặng"
        }
    }
}

enum ReChargeMethod: Int, EnumEnumerable {
    case none
    case viaAccount
    case receiveMoney
}

enum DiscountType: Int, EnumEnumerable {
    case purchase = 0 //Mua san pham
    case whosale = 1 //Ban si
    case individual = 2 //Ban le
}

enum DiscountBasedOn: Int, EnumEnumerable {
    case product = 0 // Dua tren san pham
    case orderValue = 1// Dua tren gia tri don hang
    case incomeMonthly = 2// Dua tren thu nhap thang
    case none = 3
}

enum AddProductType: Int, EnumEnumerable {
    case none = 0
    case stockCheck = 1
}
