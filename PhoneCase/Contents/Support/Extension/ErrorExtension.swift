//
//  ErrorExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import Foundation

class AppError: Error {
    
    static func networkError() -> Error {
        
        return NSError(domain: "com.app.ISG", code: AppErrorCode.network.rawValue, userInfo: [NSLocalizedDescriptionKey : "Không có kết nối mạng. Vui lòng thử lại sau"])
        
    }
    
    static func unAuthorizedError() -> Error {
        return NSError(domain: "com.app.ISG", code: AppErrorCode.unAuthorized.rawValue, userInfo: [NSLocalizedDescriptionKey: "Yêu cầu đăng nhập lại"])
    }
    
    static func timeoutError() -> Error {
        return NSError(domain: "com.app.ISG", code: AppErrorCode.timeout.rawValue, userInfo: [NSLocalizedDescriptionKey: "Quá thời gian thực thi. Vui lòng thử lại sau"])
    }
    
    static func serverConnection() -> Error {
        return NSError(domain: "com.app.ISG", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Không thể kết nối máy chủ"])
    }
    
    static func serverError() -> Error {
        return NSError(domain: "com.app.ISG", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Lỗi hệ thống, liên lạc với admin để được trợ giúp"])
    }
    
    static func commonError() -> Error {
        return NSError(domain: "com.app.ISG", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Có lỗi xảy ra. Vui lòng thử lại sau"])
    }
}
