//
//  UIViewExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

extension UIView {
    
    class func loadFromNibNamed(_ nibNamed: String?="") -> UIView? {
        
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        
        return UINib(
            nibName: className,
            bundle: nil
            ).instantiate(withOwner: self, options: nil)[0] as? UIView
    }
    
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor.clear
        }
        set (color) {
            self.layer.borderColor = color.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return 0
        }
        set (width) {
            self.layer.borderWidth = width
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return 0
        }
        set (width) {
            self.layer.cornerRadius = width
            self.layer.masksToBounds = true
        }
    }
}

