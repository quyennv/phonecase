//
//  NotificationNameExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/24/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

extension NSNotification.Name {
    static let menuButtonClicked = Notification.Name.init("menu-button-clicked")
    static let cartHasChanged = Notification.Name.init("cart-has-changed")
}
