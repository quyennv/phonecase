//
//  NSNumberExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/25/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

extension NSNumber {
    
    func priceString(_ enterLine: Bool? = false) -> String {
        if enterLine! {
            return Utilities.formatPriceNumber(self) + "\nVNĐ"
        }
        return Utilities.formatPriceNumber(self) + " VNĐ"
    }
    
    func numberString() -> String {
        return Utilities.formatPriceNumber(self)
    }
    
    
}
