//
//  UserDefault.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 5/17/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    fileprivate enum Constants: String {
        case deviceToken
    }
    
    func currentDeviceToken() -> String? {
        guard let token = string(forKey: Constants.deviceToken.rawValue) else {
            return nil
        }
        return token
    }
    
    func setDeviceToken(_ token: String) {
        set(token, forKey: Constants.deviceToken.rawValue)
    }
    
    
}
