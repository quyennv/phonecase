//
//  UIViewControllerExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit
import AVFoundation
import NVActivityIndicatorView

enum StoryBoard: String {
    case main = "Main"
    case auth = "Auth"
    case home = "Home"
    case cart = "Cart"
    case search = "Search"
    case setting = "Setting"
    case common = "Common"
    case appFeature = "AppFeature"
    case income = "Income"
}

extension UIViewController {
    
    static func viewController(_ storyboard: StoryBoard) -> UIViewController? {
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: className)
    }
    
    func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view.isKind(of: UIImageView.self) && view.bounds.size.height <= 1.0 {
            return view as? UIImageView
        }
        
        for subview in view.subviews {
            if let imgView = self.findHairlineImageViewUnder(subview){
                return imgView
            }
        }
        return nil
    }
    
    func showProgress() {
        let activityData = ActivityData(type: .ballClipRotate)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func dismissProgress() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    
    func showAlert(_ error: Error) {
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
        DispatchQueue.main.async {
            let errorCode = (error as NSError).code
            if errorCode == AppErrorCode.unAuthorized.rawValue {
                
                let alert = ISGAlertController(title: "Thông báo", message: error.localizedDescription, completion: { (_, _) in
                    
                    //TODO: Set root navi
                }, cancelButtonTitle: "Đăng nhập", otherButtonTitle: nil)
                alert.show(viewController: self, completion: nil)

            } else {
                
                let alert = ISGAlertController(title: "Thông báo", message: error.localizedDescription, completion: nil, cancelButtonTitle: "Đóng", otherButtonTitle: nil)
                alert.show(viewController: self, completion: nil)
                
            }
            
        }
    }
    
    
    
    func showAlertWithMessage(_ message: String) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
        let alert = ISGAlertController(title: "Thông báo", message: message, completion: nil, cancelButtonTitle: "Đóng", otherButtonTitle: nil)
        alert.show(viewController: self, completion: nil)
    }
    
    func doShare(_ urlString: String) {
        
        let url = URL(string: urlString)
        let shareItems = [url!]
        let activityController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityController.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
        self.present(activityController, animated: true, completion: nil)
    }
    
    func initDatePicker() -> UIDatePicker {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "vi_VN")
        datePicker.calendar = Calendar(identifier: .gregorian)
        
        return datePicker
    }
    
    func initToolbar() -> UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        let cancelItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(doCancelItem))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "Chọn", style: .plain, target: self, action: #selector(doDoneItem))
        toolBar.setItems([cancelItem, spaceItem, doneItem], animated: false)
        
        return toolBar
    }
    
    func doCancelItem() {
        
    }
    
    func doDoneItem() {
        
    }
    
    func setupLoadMoreIndicator() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60)
        indicator.startAnimating()
        return indicator
    }
    
    func doRefresh() {
        
    }
    
    func doCall(_ phone: String) {
        guard let url = URL(string: "tel://\(phone)") else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        } else {
            
            let alertController = ISGAlertController(title: "Thông báo", message: "Thiết bị không hỗ trợ chức năng này", completion: nil, cancelButtonTitle: nil, otherButtonTitle: "Đóng")
            alertController.show(viewController: self, completion: nil)
        }
        
    }
    
    func doMessage(_ messageComposer: ISGMessageComposer, _ phone: String) {
        
        if messageComposer.canSendText() {
            let messageComposerViewController = messageComposer.configMessageComposeViewController(phone)
            present(messageComposerViewController, animated: true, completion: nil)
        } else {
            showAlertWithMessage("Không thể gửi tin nhắn")
        }
    }
    
    func doSelectPhoto() {
        let actionSheet = UIAlertController(title: "Chọn ảnh", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Thư viện ảnh", style: .default, handler: { (action: UIAlertAction) in
            self.doSelectMediaPhotoWithType(.photoLibrary)
        }))
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(title: "Chụp ảnh", style: .default, handler: { (action: UIAlertAction) in
                self.doSelectMediaPhotoWithType(.camera)
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    func doSelectMediaPhotoWithType(_ sourceType: UIImagePickerControllerSourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = sourceType
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func addSearchBar(_ title: String?=nil, _ enable: Bool?=true) {
        let searchBar = UISearchBar()
        searchBar.text = title
        searchBar.placeholder = "Tìm kiếm trên iShopGo"
        searchBar.backgroundImage = UIImage()
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        if enable! {
            searchBar.becomeFirstResponder()
        } else {
            searchBar.isUserInteractionEnabled = false
        }
        
    }
    
    //MARK: POP UP
    func presentPopupWithController(popupViewController: UIViewController, completed: @escaping () -> Void) {
        
        self.tabBarController?.addChildViewController(popupViewController)
        
        var window = UIApplication.shared.keyWindow
        if window == nil {
           window = UIApplication.shared.windows.first
        }
        
        let blurView = UIView(frame: (window?.bounds)!)
        blurView.backgroundColor = .black
        blurView.alpha = 0.0
        blurView.tag = 1111
        blurView.isExclusiveTouch = true
        blurView.isMultipleTouchEnabled = false
        //Gesture
        let dismissTapGeture = UITapGestureRecognizer(target: self, action: #selector(dismissGesture))
        blurView.addGestureRecognizer(dismissTapGeture)
        
        self.tabBarController?.view.addSubview(blurView)
        
        UIView.animate(withDuration: 0.2) { 
            blurView.alpha = 0.7
        }
        
        var heightPopupView = popupViewController.view.frame.size.height - 100
        var widthPopupView = popupViewController.view.frame.size.width - 40
        
        let height = window?.bounds.size.height
        let width = window?.bounds.size.width
        
        if heightPopupView > height! {
            heightPopupView = height! - 100
        }
        
        if widthPopupView > width! {
            widthPopupView = width!
        }
        
        popupViewController.view.frame = CGRect(x: (width! - widthPopupView) / 2 , y: height!, width: widthPopupView, height: heightPopupView)
        
        popupViewController.view.tag = 1112
        
        self.tabBarController?.view.addSubview(popupViewController.view)
        popupViewController.view.layer.cornerRadius = 5
        popupViewController.view.layer.masksToBounds = true
        
        popupViewController.view.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions(rawValue: 0), animations: { 
            
            popupViewController.view.alpha = 1
            popupViewController.view.frame = CGRect(x: popupViewController.view.frame.origin.x, y: (height! - heightPopupView) / 2, width: popupViewController.view.frame.size.width, height: popupViewController.view.frame.size.height)
        }) { (finished) in
            completed()
        }
    }
    
    func presentPopupWithView(popupView: UIView, completed: @escaping () -> Void) {
        
        var window = UIApplication.shared.keyWindow
        if window == nil {
            window = UIApplication.shared.windows.first
        }
        
        let blurView = UIView(frame: (window?.bounds)!)
        blurView.backgroundColor = .black
        blurView.alpha = 0.0
        blurView.tag = 1111
        blurView.isExclusiveTouch = true
        blurView.isMultipleTouchEnabled = false
        //Gesture
        let dismissTapGeture = UITapGestureRecognizer(target: self, action: #selector(dismissGesture))
        blurView.addGestureRecognizer(dismissTapGeture)
        
        self.tabBarController?.view.addSubview(blurView)
        
        UIView.animate(withDuration: 0.2) {
            blurView.alpha = 0.7
        }
        
        var heightPopupView = popupView.frame.size.height
        var widthPopupView = popupView.frame.size.width
        
        let height = window?.bounds.size.height
        let width = window?.bounds.size.width
        
        if heightPopupView > height! {
            heightPopupView = height! - 100
        }
        
        if widthPopupView > width! {
            widthPopupView = width!
        }
        
        popupView.frame = CGRect(x: (width! - widthPopupView) / 2 , y: height!, width: widthPopupView, height: heightPopupView)
        
        popupView.tag = 1112
        
        self.tabBarController?.view.addSubview(popupView)
        popupView.layer.cornerRadius = 5
        popupView.layer.masksToBounds = true
        
        popupView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions(rawValue: 0), animations: {
            
            popupView.alpha = 1
            popupView.frame = CGRect(x: popupView.frame.origin.x, y: (height! - heightPopupView) / 2 - 50, width: popupView.frame.size.width, height: popupView.frame.size.height)
        }) { (finished) in
            completed()
        }
    }

    func dismissGesture() {
     
        dismissPopup {
        }
    }
    
    func dismissPopup(completed: @escaping () -> Void) {
        var window = UIApplication.shared.keyWindow
        if window == nil {
            window = UIApplication.shared.windows.first
        }
        for controller in (self.tabBarController?.viewControllers)! {
            if controller.view.tag == 1112 {
                controller.removeFromParentViewController()
            }
        }
        
        let removeGroup = DispatchGroup()
        for aView in (self.tabBarController?.view.subviews)! {
            
            if aView.tag == 1112 {
                
                removeGroup.enter()
                UIView.animate(withDuration: 0.2, animations: {
                    aView.frame = CGRect(x: aView.frame.origin.x, y: (window?.frame.size.height)!, width: aView.frame.size.width, height: aView.frame.size.height)
                    self.view.layoutIfNeeded()
                    self.view.setNeedsLayout()
                    
                }, completion: { (finished) in
                    aView.removeFromSuperview()
                    removeGroup.leave()
                })
            }
            
            if aView.tag == 1111 {
                removeGroup.enter()
                UIView.animate(withDuration: 0.5, animations: { 
                    aView.alpha = 0.0
                    self.view.layoutIfNeeded()
                    self.view.setNeedsLayout()
                }, completion: { (finished) in
                    removeGroup.leave()
                    aView.removeFromSuperview()
                })
            }
        }
        
        removeGroup.notify(queue: .main) { 
            completed()
        }
    }
}

extension UIViewController: UISearchBarDelegate {
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
        
        // todo
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
        //todo
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        
        //todo
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        // todo
    }
}

extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
    }
}


