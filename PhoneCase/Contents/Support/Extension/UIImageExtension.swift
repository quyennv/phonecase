//
//  UIImageExtension.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/26/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit

extension UIImage {
    func dataForUpload(fileSize: CGFloat) -> Data {
        
        var imageData = UIImageJPEGRepresentation(self, 1.0)
        var compressionRate: CGFloat = 10
        
        while imageData!.count > Int(1024 * fileSize) {
            if compressionRate > 0.5 {
                compressionRate -= 0.5
                imageData = UIImageJPEGRepresentation(self, compressionRate/10)
            } else {
                return imageData!
            }
        }
        
        return imageData!
        
    }
}
