//
//  Utilities.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 4/23/17.
//  Copyright © 2017 Mua Do Tot. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class Utilities: NSObject {
    
    class func isNilOrEmpty(_ object: Any?) -> Bool {
        if(object == nil) {
            return true
        }
        if(object is String && (object as! String).isEmpty){
            return true
        }
        if(object is Array<Any> && (object as! Array<Any>).count == 0){
            return true
        }
        if(object is Dictionary<String, Any> && (object as! Dictionary<String, Any>).count == 0){
            return true
        }
        return false
    }
    
    class func formatPriceNumber(_ price: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        numberFormatter.groupingSeparator = "."
        
        return numberFormatter.string(from: price)!
    }
    
    class func isEmail(_ email: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4}$", options: NSRegularExpression.Options.caseInsensitive)
            let matches = regex.matches(in: email, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, email.characters.count))
            return matches.count > 0
            
        } catch {
            return false
        }
    }
    
    class func isPhone(_ value: String) -> Bool {
        let phoneRegex = "^((\\+84)|(84)|(0))[0-9]{9,10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: value)
    }
    
    class func videoPreviewUIImage(_ videoURL: URL) -> UIImage? {
        
        let asset = AVURLAsset(url: videoURL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let timestamp = CMTime(seconds: 2, preferredTimescale: 60)
        
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
    
    class func sizeForNavigationCell(_ text: String) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 500, height: 30))
        label.text = text
        label.font = UIFont.systemFont(ofSize: 14)
        label.sizeToFit()
        return label.frame.size.width
    }
}
